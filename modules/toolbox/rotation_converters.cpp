#include <iostream>

#include "rotation_converters.h"

void outputAsMatrix(const Eigen::Quaterniond& q)
{
    Eigen::Matrix3d rot_mat = q.normalized().toRotationMatrix();

    std::cout << "R=" << std::endl << rot_mat << std::endl;

    // Vector3d
    // Eigen::Matrix<double, 3, 1, 0, 3, 1> euler =
    // q.toRotationMatrix().eulerAngles(0, 1, 2);

    auto euler = rot_mat.eulerAngles(0, 1, 2);

    std::cout << "Euler from quaternion in roll, pitch, yaw (in radians)" << std::endl << euler << std::endl;

    // std::cout << "Variable type " << std::endl ;
    // std::cout << type_name<decltype(euler)>() << '\n';
}
