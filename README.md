# PiCalculations

PiCalculations provides functions based on infinite series or products to approximate the constant pi

<!-- vim-markdown-toc GitLab -->

* [Installation](#installation)
    * [Build](#build)
    * [Create Conan package](#create-conan-package)
* [Algorithm](#algorithm)

<!-- vim-markdown-toc -->

## Installation

### Build

To build this project you must have `conan`, `cmake` and a compiler that support cpp17.

```bash
git clone https://gitlab.com/cpp-training/pi_calculations
mkdir build
cd build
conan install ../pi_calculations -s compiler.cppstd=17 -b missing
cmake ../pi_calculations
make
ctest
```

### Create Conan package

```bash
conan create . -s compiler.cppstd=17 -b missing
```

## Algorithm

[Pi_wiki_en](https://en.wikipedia.org/wiki/Pi)

[Convergences compared](https://en.wikipedia.org/wiki/File:Comparison_pi_infinite_series.svg)

[Chudnovsky_approx](https://en.wikipedia.org/wiki/Chudnovsky_algorithm)


blabla ...

