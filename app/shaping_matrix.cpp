#include <array>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <memory>

// #include <toolbox/rotation_converters.h>

#include "shaping_matrix.h"

int main()
{
    std::cout << "===================================================" << std::endl;
    std::cout << "--> Demo app: reshape_matrix" << std::endl;
    reshape_matrix();

    std::cout << "===================================================" << std::endl;
    std::cout << "--> Demo app: convertArrayToMatrix" << std::endl;
    convertArrayToMatrix();

    std::cout << "===================================================" << std::endl;
    std::cout << "--> Demo app: homogenousInv" << std::endl;
    homogenousInv();
}

void reshape_matrix()
{
    // Will be declared as an Eigen::ColMajor matrix by default
    Eigen::Matrix<float, 3, 4> some_matrix;
    some_matrix << 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12;

    // https://eigen.tuxfamily.org/dox/group__TutorialBlockOperations.html
    Eigen::Matrix<float, 3, 4, Eigen::RowMajor> row_major_matrix;
    row_major_matrix.setZero();

    // Conversion to RowMajor order is implicit through matrix declaration
    for (uint16_t irow = 0; irow < some_matrix.rows(); irow++)
    {
        row_major_matrix.row(irow) = some_matrix.row(irow);
    }

    std::cout << "Storage order: " << (some_matrix.IsRowMajor ? "RowMajor" : "ColMajor") << std::endl;
    std::cout << "ColMajor matrix = \n" << some_matrix << std::endl;

    std::cout << "Storage order: " << (row_major_matrix.IsRowMajor ? "RowMajor" : "ColMajor") << std::endl;
    std::cout << "RowMajor matrix = \n" << row_major_matrix << std::endl;

    std::cout << "Matrix are identical but ColMajor data is stored as follows" << std::endl;
    Eigen::Map<Eigen::VectorXf> data_col_major(some_matrix.data(), some_matrix.size());
    std::cout << "ColMajor data  = \n" << data_col_major << std::endl;

    std::cout << "Whereas RowMajor data is stored as follows" << std::endl;
    Eigen::Map<Eigen::VectorXf> data_row_major(row_major_matrix.data(), row_major_matrix.size());
    std::cout << "RowMajor data = \n" << data_row_major << std::endl;
}

void convertArrayToMatrix()
{

    using namespace Eigen;

    std::array<double, 12> l_values{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };

    std::cout << "Let us initialize matrices with the following vector data:" << std::endl;
    std::cout << Map<Eigen::VectorXd>(l_values.data(), l_values.size()).transpose() << std::endl;

    MatrixXd Amat_col_major = Map<Matrix<double, 3, 4, ColMajor>>(l_values.data());
    MatrixXd Amat_row_major = Map<Matrix<double, 3, 4, RowMajor>>(l_values.data());

    std::cout << "Off course if matrices are initialized with the same vector... " << std::endl;
    std::cout << "Column-major:\n" << Amat_col_major << std::endl;
    std::cout << "Row-major:\n" << Amat_row_major << std::endl;
    std::cout << "...few unwanted surprises might occur.\n" << std::endl;

    Eigen::Matrix<double, 3, 4, Eigen::RowMajor> l_rowmajor_matrix(l_values.data());

    Eigen::Matrix<double, 3, 4, Eigen::ColMajor> l_colmajor_copy;

    for (uint16_t irow = 0; irow < l_rowmajor_matrix.rows(); irow++)
    {
        l_colmajor_copy.row(irow) = l_rowmajor_matrix.row(irow);
    }

    std::cout << "This error can be avoided with a row-wise copy :" << std::endl;
    std::cout << "Row-wise copy of the Column-major matrix:\n" << l_colmajor_copy << std::endl;
}

/**
 * Simple verification of the inverse of an homogeneous matrix
 *
 * https://mathematica.stackexchange.com/questions/106257/how-do-i-get-the-inverse-of-a-homogeneous-transformation-matrix
 *
 */
void homogenousInv()
{

    Eigen::Matrix<float, 4, 4, Eigen::ColMajor> H;
    H << 0.64633053, -0.73017417, -0.22159089, 0, 0.71782278, 0.48331409, 0.50113666, 0, -0.25881905, -0.48296291,
      0.8365163, 0, 0.0, 0.0, 0.0, 1.0;

    std::cout << "Original transformation, H = \n" << H << std::endl;

    Eigen::Matrix4f Hinv(Eigen::Matrix4f::Identity());

    Hinv.block<3, 3>(0, 0) = H.block<3, 3>(0, 0).transpose();
    Hinv.block<3, 1>(0, 3) = -Hinv.block<3, 3>(0, 0) * H.block<3, 1>(0, 3);

    std::cout << "H inv (with smart algebra) = \n" << Hinv << std::endl;

    std::cout << "H.inverse() = \n" << H.inverse() << std::endl;

    std::cout << "Is that correct ?" << std::endl;
    std::cout << "Hinv * H = \n" << Hinv * H << std::endl;
}
