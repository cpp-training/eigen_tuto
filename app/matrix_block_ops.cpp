#include <array>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <memory>

#include <Eigen/Dense>
#include <Eigen/Geometry>

#include "matrix_block_ops.h"

int main()
{
    const float l_L = 3.14159;

    Eigen::Matrix<float, 3, 4, Eigen::ColMajor> l_board;
    l_board << 0., l_L, l_L, 0., 0., 0., l_L, l_L, 0., 0., 0., 0.;

    Eigen::Vector3f l_trl;
    l_trl << 1, 2, 3;

    // Broadcast the vector to match the matrix dimensions
    Eigen::Matrix<float, 3, 4, Eigen::ColMajor> l_trl_broadcasted = l_trl.rowwise().replicate(4);

    Eigen::Matrix<float, 3, 4, Eigen::ColMajor> l_total = l_board + l_trl_broadcasted;

    // std::cout << l_total << std::endl;

    matrix_block_ops();
}

void matrix_block_ops()
{
    Eigen::MatrixXf m(4, 4);
    m << 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16;

    // just change 1 element
    m(2, 3) = 3.141;

    std::cout << "Full matrix" << std::endl;
    std::cout << m << std::endl << std::endl;

    std::cout << "Block in the middle" << std::endl;
    std::cout << m.block<2, 2>(1, 1) << std::endl << std::endl;

    for (int i = 1; i <= 3; ++i)
    {
        std::cout << "Block of size " << i << "x" << i << std::endl;
        std::cout << m.block(0, 0, i, i) << std::endl << std::endl;
    }

    std::cout << m.block(0, 3, 3, 1) << std::endl << std::endl;
}
