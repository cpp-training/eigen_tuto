#include <array>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <memory>

#include <toolbox/rotation_converters.h>

#include "quaternion_ops.h"

int main()
{
    quaternion_ops();
}

// full link:
/* https://stackoverflow.com/questions/36501262/
quaternion-to-rotation-matrix-incorrect-values-using-eigen-library */
void quaternion_ops()
{
    float angle = M_PI / 4.;
    auto sinA = std::sin(angle / 2.);
    auto cosA = std::cos(angle / 2.);

    Eigen::Quaterniond q;
    q.x() = 0 * sinA;
    q.y() = 0 * sinA;
    q.z() = 1 * sinA;
    q.w() = cosA;

    quaternion_display(q);
    outputAsMatrix(q);

    std::cout << "Or equivalently with Angle-axis initialisation" << std::endl;

    Eigen::Quaterniond q2{ Eigen::AngleAxisd{ angle, Eigen::Vector3d{ 0., 0., 1. } } };

    quaternion_display(q2);
    outputAsMatrix(q2);
}

void quaternion_display(Eigen::Quaterniond const& q)
{
    std::cout << "This quaternion consists of a scalar " << std::endl
              << q.w() << std::endl
              << " and a vector " << std::endl
              << q.vec() << std::endl;
}
