#pragma once 

#include <Eigen/Dense>
#include <Eigen/Geometry>


void quaternion_ops();
void quaternion_display(Eigen::Quaterniond const &q);

